﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NekosdotNet;

namespace NekosTestapp
{
    public class Program
    {
        private static void Main(string[] args)
        {
            new Program().MainAsync().GetAwaiter().GetResult();
        }

        private async Task MainAsync()
        {
            NekosClient nekosClient = new NekosClient();

            Console.WriteLine((await nekosClient.Image.GetSfwImage()).Url);
            Console.WriteLine((await nekosClient.Image.GetPat()).Url);
            Console.WriteLine((await nekosClient.Image.GetHug()).Url);
            Console.WriteLine((await nekosClient.Image.GetKiss()).Url);
            Console.WriteLine((await nekosClient.Image.GetSlap()).Url);
            Console.WriteLine((await nekosClient.Image.GetPoke()).Url);
            Console.WriteLine((await nekosClient.Image.GetLizard()).Url);

            Console.ReadKey();
        }
    }
}
