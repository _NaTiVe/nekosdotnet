﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using NekosdotNet.Responses;
using NekosdotNet.Helpers;

namespace NekosdotNet.Endpoints
{
    public class Image
    {
        public async Task<ImageRES> GetSfwImage(string endpoint)
        {
            if (ImageEndpoints.Nsfw.Contains(endpoint))
                throw new ArgumentException("Endpoint is in NSFW!");

            if (string.IsNullOrEmpty(endpoint))
                throw new ArgumentException("Endpoint can't be empty!");

            return await Universal.GET<ImageRES>($"img/{endpoint}");
        }

        public async Task<ImageRES> GetSfwImage()
        {
            Random random = new Random();
            return await Universal.GET<ImageRES>($"img/{ImageEndpoints.Sfw[random.Next(ImageEndpoints.Sfw.Length - 1)]}");
        }

        public async Task<ImageRES> GetNsfwImage(string endpoint)
        {
            if (ImageEndpoints.Sfw.Contains(endpoint))
                throw new ArgumentException("Endpoint is in SFW!");

            if (string.IsNullOrEmpty(endpoint))
                throw new ArgumentException("Endpoint can't be empty!");

            return await Universal.GET<ImageRES>($"img/{endpoint}");
        }

        public async Task<ImageRES> GetNsfwImage()
        {
            Random random = new Random();
            return await Universal.GET<ImageRES>($"img/{ImageEndpoints.Nsfw[random.Next(ImageEndpoints.Sfw.Length - 1)]}");
        }

        public async Task<ImageRES> GetHug()
        {
            return await Universal.GET<ImageRES>("img/hug");
        }

        public async Task<ImageRES> GetKiss()
        {
            return await Universal.GET<ImageRES>("img/kiss");
        }

        public async Task<ImageRES> GetPat()
        {
            return await Universal.GET<ImageRES>("img/pat");
        }

        public async Task<ImageRES> GetLizard()
        {
            return await Universal.GET<ImageRES>("img/lizard");
        }

        public async Task<ImageRES> GetSlap()
        {
            return await Universal.GET<ImageRES>("img/slap");
        }

        public async Task<ImageRES> GetPoke()
        {
            return await Universal.GET<ImageRES>("img/poke");
        }
    }
}
