﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using NekosdotNet.Responses;
using NekosdotNet.Helpers;

namespace NekosdotNet.Endpoints
{
    public class Deprecated
    {
        public async Task<HugRES> GetHug()
        {
            return await Universal.oldGET<HugRES>("hug");
        }

        public async Task<KissRES> GetKiss()
        {
            return await Universal.oldGET<KissRES>("kiss");
        }

        public async Task<LizardRES> GetLizard()
        {
            return await Universal.oldGET<LizardRES>("lizard");
        }

        public async Task<PatRES> GetPat()
        {
            return await Universal.oldGET<PatRES>("pat");
        }

        public async Task<NekoRES> GetNeko()
        {
            return await Universal.oldGET<NekoRES>("neko");
        }
    }
}
