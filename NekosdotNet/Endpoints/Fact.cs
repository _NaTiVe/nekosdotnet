﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using NekosdotNet.Responses;
using NekosdotNet.Helpers;

namespace NekosdotNet.Endpoints
{
    public class Fact
    {
        public async Task<FactRES> GetFact()
        {
            return await Universal.GET<FactRES>("fact");
        }
    }
}
