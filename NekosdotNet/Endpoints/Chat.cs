﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using NekosdotNet.Responses;
using NekosdotNet.Helpers;

namespace NekosdotNet.Endpoints
{
    public class Chat
    {
        public async Task<ChatRES> GetChat(string chat)
        {
            if (chat.Length > 200)
                throw new ArgumentException("Chat message can't be over 200 characters long!");

            return await Universal.GET<ChatRES>($"chat?text={Uri.EscapeUriString(chat)}");
        }
    }
}
