﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using NekosdotNet.Responses;
using NekosdotNet.Helpers;

namespace NekosdotNet.Endpoints
{
    public class Owoify
    {
        public async Task<OwoifyRES> GetOwoify(string text)
        {
            if (text.Length > 200)
                throw new ArgumentException("Owoify text can't be over 200 characters long!");

            return await Universal.GET<OwoifyRES>($"owoify?text={Uri.EscapeUriString(text)}");
        }
    }
}
