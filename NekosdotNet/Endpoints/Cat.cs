﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using NekosdotNet.Responses;
using NekosdotNet.Helpers;

namespace NekosdotNet.Endpoints
{
    public class Cat
    {
        public async Task<CatRES> GetCat()
        {
            return await Universal.GET<CatRES>("cat");
        }
    }
}
