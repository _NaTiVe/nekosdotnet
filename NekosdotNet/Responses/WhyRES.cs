﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NekosdotNet.Responses
{
    public class WhyRES
    {
        [JsonProperty(PropertyName = "why")]
        public string Why { get; set; }
    }
}
