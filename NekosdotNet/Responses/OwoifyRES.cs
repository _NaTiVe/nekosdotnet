﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NekosdotNet.Responses
{
    public class OwoifyRES
    {
        [JsonProperty(PropertyName = "msg")]
        public string Error { get; set; }
        [JsonProperty(PropertyName = "owo")]
        public string OwoifiedText { get; set; }
    }
}
