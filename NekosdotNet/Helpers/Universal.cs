﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;

namespace NekosdotNet.Helpers
{
    public class Universal
    {
        public const string baseUrl = "https://nekos.life/api/v2/";
        public const string oldBaseUrl = "https://nekos.life/api/";

        public static async Task<T> GET<T>(string url)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(baseUrl);

                HttpResponseMessage res = await httpClient.GetAsync(url);

                if (res.StatusCode != HttpStatusCode.OK)
                    throw new HttpRequestException($"Oh oh! Something went wrong.. {res.StatusCode} | {res.ReasonPhrase}");

                string json = await res.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<T>(json);
            }
        }

        public static async Task<T> oldGET<T>(string url)
        {
            using (HttpClient oldHttpClient = new HttpClient())
            {
                oldHttpClient.BaseAddress = new Uri(oldBaseUrl);

                HttpResponseMessage res = await oldHttpClient.GetAsync(url);

                if (res.StatusCode != HttpStatusCode.OK)
                    throw new HttpRequestException($"Oh oh! Something went wrong.. {res.StatusCode} | {res.ReasonPhrase}");

                string json = await res.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<T>(json);
            }
        }
    }
}
