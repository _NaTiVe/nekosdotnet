﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

using NekosdotNet.Endpoints;

namespace NekosdotNet
{
    public class NekosClient
    {
        public EightBall EightBall { get; set; }
        public Cat Cat { get; set; }
        public Chat Chat { get; set; }
        public Deprecated Deprecated { get; set; }
        public Fact Fact { get; set; }
        public Image Image { get; set; }
        public Owoify Owoify { get; set; }
        public Why Why { get; set; }

        public NekosClient()
        {
            EightBall = new EightBall();
            Cat = new Cat();
            Chat = new Chat();
            Deprecated = new Deprecated();
            Fact = new Fact();
            Image = new Image();
            Owoify = new Owoify();
            Why = new Why();
        }
    }
}
