﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NekosdotNet.Responses
{
    public class NekoRES
    {
        [JsonProperty(PropertyName = "neko")]
        public string Url { get; set; }
    }
}
